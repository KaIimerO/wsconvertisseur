﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WSConvertisseur.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WSConvertisseur.Controllers
{
    [Route("api/devises")]
    public class DeviseController : Controller
    {

        private List<Devise> devises = new List<Devise>();

        public DeviseController()
        {
            devises.Add(new Devise(1, "Dollar", 1.08));
            devises.Add(new Devise(2, "Franc Suisse", 1.07));
            devises.Add(new Devise(3, "Yen", 120));
        }

        /// <summary>
        /// Get a list of currencies.
        /// </summary>
        /// <returns>Http response</returns>
        /// <response code="200">A list of currencies or an empty list</response> 
        // GET: api/devises
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Devise>), 200)]
        public async Task<IEnumerable<Devise>> GetAll()
        {
            return devises;
        }

        /// <summary>
        /// Get a single currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <param name="id">The id of the currency</param>
        /// <response code="200">When the currency id is found</response> 
        /// <response code="404">When the currency id is not found</response>
        // GET api/devises/5
        [HttpGet("{id}", Name = "GetDevise")]
        [ProducesResponseType(typeof(Devise), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetById(int id)
        {
            Devise devise = devises.FirstOrDefault((d) => d.Id == id);
            if (devise == null)
            {
                return NotFound();
            } 
            return Ok(devise);
        }

        /// <summary>
        /// Create a currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <response code="201">When the currency created</response>
        /// <response code="400">When the currency sent is not valid</response>
        // POST api/devises
        [HttpPost]
        [ProducesResponseType(typeof(Devise), 201)]
        [ProducesResponseType(400)]
        public async Task<IActionResult> Post([FromBody]Devise devise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            devises.Add(devise);
            return CreatedAtRoute("GetDevise", new { id = devise.Id }, devise);
        }

        /// <summary>
        /// Upate a currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <response code="204">When the currency is updated</response>
        /// <response code="400">When the currency is not valid</response>
        /// <response code="404">When the currency id is not found</response>
        // PUT api/devises/5
        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Put(int id, [FromBody] Devise devise)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != devise.Id)
            {
                return BadRequest();
            }
            int index = devises.FindIndex((d) => d.Id == id); if (index < 0)
            {
                return NotFound();
            }
            devises[index] = devise;
            return NoContent();
        }

        /// <summary>
        /// Delete a currency.
        /// </summary>
        /// <returns>Http response</returns>
        /// <response code="200">When the currency is deleted</response>
        /// <response code="404">When the currency id is not found</response>
        // DELETE api/devises/5
        [HttpDelete("{id}")]
        [ProducesResponseType(typeof(Devise), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            Devise devise = devises.FirstOrDefault((d) => d.Id == id);
            if (devise == null)
            {
                return NotFound();
            }
            devises.Remove(devise);
            return Ok(devise);
        }
    }
}
