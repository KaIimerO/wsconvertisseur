﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WSConvertisseur.Models
{
    public class Devise
    {
        public int Id
        {
            get;
            set;
        }

        [Required]
        public string NomDevise
        {
            get;
            set;
        }

        [Required]
        public double Taux
        {
            get;
            set;
        }

        public Devise()
        {
        }

        public Devise(int id, string nomDevise, double taux)
        {
            Id = id;
            NomDevise = nomDevise;
            Taux = taux;
        }

        public override bool Equals(object obj)
        {
            var devise = obj as Devise;
            return devise != null &&
                   Id == devise.Id &&
                   NomDevise == devise.NomDevise &&
                   Taux == devise.Taux;
        }
    }
}
